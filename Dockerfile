FROM golang:latest AS builder

ENV GO111MODULE=on

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOBIN=/app/bin go install ./...

FROM alpine:latest AS runner
RUN apk add --no-cache ca-certificates

COPY --from=builder /app/bin /usr/local/bin

ENTRYPOINT [ "/usr/local/bin/tiktokpirate" ]
