package main

import (
	"encoding/base64"
	"encoding/json"
	"html/template"
	"net/http"
)

type vidPreviewData struct {
	Title            string `json:"t,omitempty"`
	SiteName         string `json:"n,omitempty"`
	VideoURL         string `json:"u,omitempty"`
	VideoContentType string `json:"c,omitempty"`
	Description      string `json:"d,omitempty"`
}

var vidPreviewTmpl = template.Must(template.New("video preview").Parse(`
<!DOCTYPE html>
<html>
<head>
<title>{{ .Title }}</title>
<meta property="og:site_name" content="{{ .SiteName }}" />
<meta property="og:title" content="{{ .Title }}" />
<meta property="og:description" content="{{ .Description }}" />
<meta property="og:type" content="video" />
<meta property="og:video" content="{{ .VideoURL }}" />
<meta property="og:video:type" content="{{ .VideoContentType }}" />
</head>
<body>
	<h1>{{ .SiteName }}</h1>
	<h2>{{ .Title }}</h2>
	<video>
		<source src="{{ .VideoURL }}" type="{{ .VideoContentType }}" />
	</video>
</body>
</html>
`))

func handleVidPreview(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query().Get("p")
	qDec, err := base64.URLEncoding.DecodeString(q)
	if err != nil {
		http.Error(w, "invalid p: "+err.Error(), http.StatusBadRequest)
	}

	var data vidPreviewData
	err = json.Unmarshal(qDec, &data)
	if err != nil {
		http.Error(w, "invalid p json: "+err.Error(), http.StatusBadRequest)
	}

	err = vidPreviewTmpl.Execute(w, data)
	if err != nil {
		http.Error(w, "template error: "+err.Error(), http.StatusInternalServerError)
	}
}

func getPreviewURL(base string, data *vidPreviewData) (string, error) {
	jData, err := json.Marshal(data)
	if err != nil {
		return "", err
	}

	bData := base64.URLEncoding.EncodeToString(jData)
	return base + "?p=" + bData, nil
}
