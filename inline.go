package main

import (
	"errors"
	"log"
	"net/url"

	tb "gopkg.in/tucnak/telebot.v2"
)

const (
	defaultCacheTime = 120
)

func (b *bot) handleLinkQuery(u *url.URL) (*tb.QueryResponse, error) {
	info, err := b.crawl(u, false)
	if err != nil {
		return nil, err
	}
	vid := info.Video
	if vid == nil {
		return nil, errors.New("cant get video")
	}

	pURL, err := getPreviewURL(b.VidPreviewURL, vid.asPreviewData(b))
	if err != nil {
		return nil, err
	}

	cPreview := tb.InputMessageContent(&tb.InputTextMessageContent{
		Text:           "[@" + vid.AuthorUsername + "](" + pURL + ")",
		DisablePreview: false,
		ParseMode:      tb.ModeMarkdown,
	})

	var infoParts []messagePart
	appendVideoInfoParts(&infoParts, vid)

	var dumpParts []messagePart
	appendVideoPart(&dumpParts, vid)
	dumpParts = append(dumpParts, infoParts...)
	appendQueryParts(&dumpParts, info.Resolved.Query())
	cDump := tb.InputMessageContent(&tb.InputTextMessageContent{
		Text:           mergeParts(dumpParts),
		DisablePreview: false,
		ParseMode:      string(tb.ModeMarkdown),
	})

	_ = cPreview
	_ = cDump

	return &tb.QueryResponse{
		Results: []tb.Result{
			&tb.ArticleResult{
				ResultBase: tb.ResultBase{
					ID:      "preview",
					Content: &cPreview,
				},
				Title:       "Get",
				Description: vid.Description,
				URL:         pURL,
				HideURL:     true,
			},
			&tb.ArticleResult{
				ResultBase: tb.ResultBase{
					ID:      "dump",
					Content: &cDump,
				},
				Title:       "Dump",
				Description: vid.Description,
				URL:         pURL,
				HideURL:     true,
			},
			&tb.VideoResult{
				ResultBase: tb.ResultBase{
					ID: "video",
				},
				Title:       "Video",
				Description: vid.Description,
				Caption:     mergeParts(infoParts),
				ParseMode:   tb.ModeMarkdown,
				URL:         vid.URL,
				ThumbURL:    "https://example.com/",
				MIME:        videoMimeType,
			},
			&tb.VideoResult{
				ResultBase: tb.ResultBase{
					ID: "video_no_cap",
				},
				Title:       "Video (no caption)",
				Description: vid.Description,
				URL:         vid.URL,
				ThumbURL:    "https://example.com/",
				MIME:        videoMimeType,
			},
		},
	}, nil
}

func (b *bot) queryErrorText(q *tb.Query, text string) error {
	return b.Answer(q, &tb.QueryResponse{
		SwitchPMText: text,
	})
}

func (b *bot) onQuery(q *tb.Query) {
	u, err := b.extractLink(q.Text)
	if err != nil {
		_ = b.queryErrorText(q, "invalid link: "+err.Error())
		return
	}

	res, err := b.handleLinkQuery(u)
	if err != nil {
		_ = b.queryErrorText(q, "invalid link: "+err.Error())
		return
	}

	err = b.Answer(q, res)
	if err != nil {
		log.Println("err on answer: ", err)
	}
}
