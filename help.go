package main

import (
	"fmt"

	tb "gopkg.in/tucnak/telebot.v2"
)

const (
	helpMessage = "hi, im TikTok pirate bot.\n" +
		"You can use me in Inline mode.\n" +
		"Or send me a TikTok link and I'll grab info from it.\n" +
		"usage: \n" +
		"- link -- crawl vm.tiktok.com link\n" +
		"- /get link -- crawl vm.tiktok.com in old mode\n" +
		"- /dump link -- dump all info from vm.tiktok.com link\n" +
		"inline mode usage:\n" +
		"- type `@%s https://vm.tiktok.com/ABC123/` in any chat and send right away"
)

func (b *bot) onHelp(m *tb.Message) {
	_, _ = b.Reply(m, fmt.Sprintf(helpMessage, b.Me.Username), tb.ModeMarkdown)
}
