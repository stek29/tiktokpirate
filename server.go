package main

import (
	"log"
	"net/http"
	"os"
)

const (
	listenEnv     = "LISTEN"
	defaultListen = ":8000"

	vidPreviewPattern = "/videoPreview/"
)

func getServerMux() (*http.Server, *http.ServeMux) {
	listen := os.Getenv(listenEnv)
	if listen == "" {
		listen = defaultListen
		log.Printf("%s unset, listening on default (%s)", listenEnv, defaultListen)
	}

	mux := http.NewServeMux()
	return &http.Server{
		Addr:    listen,
		Handler: mux,
	}, mux
}
