package main

import (
	"log"
	"net/http"
	"os"
)

func noRedirect(_ *http.Request, _ []*http.Request) error {
	return http.ErrUseLastResponse
}

func getClient() *http.Client {
	userAgent := os.Getenv(userAgentEnv)
	if userAgent == "" {
		log.Printf("WARN: %s is unset, default for Go would be used", userAgentEnv)
		return http.DefaultClient
	}

	return &http.Client{
		Transport: &addHeaderTransport{
			T: http.DefaultTransport,
			AddHeaders: []addHeader{
				{Key: "User-Agent", Value: userAgent},
			},
		},
	}
}

type addHeader struct {
	Key, Value string
}

type addHeaderTransport struct {
	T          http.RoundTripper
	AddHeaders []addHeader
}

func (t *addHeaderTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	for _, h := range t.AddHeaders {
		req.Header.Add(h.Key, h.Value)
	}
	return t.T.RoundTrip(req)
}
