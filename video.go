package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

const videoMimeType = "video/mp4"

func appendVideoPart(parts *[]messagePart, info *crawledVideo) {
	*parts = append(*parts, messagePart{
		name:   "Video by " + info.AuthorName,
		value:  info.URL,
		isLink: true,
	})
}

func appendVideoInfoParts(parts *[]messagePart, info *crawledVideo) {
	*parts = append(*parts, messagePart{
		name:   "@" + info.AuthorUsername,
		value:  webTikTokBaseURL + "/@" + info.AuthorUsername,
		isLink: true,
	}, messagePart{
		value: info.Description,
	}, messagePart{
		name:   "♫ " + info.MusicTitle,
		value:  webTikTokBaseURL + info.MusicHref,
		isLink: true,
	})
}

func isVideoURL(u *url.URL) bool {
	switch u.Hostname() {
	case "m.tiktok.com":
		return strings.HasPrefix(u.Path, "/v/")
	case "www.tiktok.com", "tiktok.com":
		return strings.Contains(u.Path, "/video/")
	default:
		return false
	}
}

func extractVideoURL(sel *goquery.Selection) (string, error) {
	video := sel.Find("video").First()
	if video.Length() != 1 {
		return "", fmt.Errorf("cant find video element")
	}

	vidURL, ok := video.Attr("src")
	if !ok {
		return "", fmt.Errorf("cant find video src")
	}

	return vidURL, nil
}

type crawledVideo struct {
	URL         string
	Description string

	MusicTitle string
	MusicHref  string

	AuthorName     string
	AuthorUsername string
}

func (v crawledVideo) asPreviewData(b *bot) *vidPreviewData {
	return &vidPreviewData{
		Title:            v.AuthorName + " @" + v.AuthorUsername,
		VideoURL:         v.URL,
		SiteName:         "via @" + b.Bot.Me.Username,
		VideoContentType: videoMimeType,
		Description:      v.Description,
	}
}

func crawlVideoHTML(vid *crawledVideo, sel *goquery.Selection) (err error) {
	vid.URL, err = extractVideoURL(sel)
	if err != nil {
		return fmt.Errorf("cant get video URL: %w", err)
	}

	info := sel.Find(".video-meta-info").First()
	vid.Description = info.Find(".video-meta-title").First().Text()

	aMusic := info.Find(".music-info>a").First()
	vid.MusicTitle = aMusic.Text()
	// strip query args
	vid.MusicHref = strings.SplitN(aMusic.AttrOr("href", ""), "?", 2)[0]

	userInfo := sel.Find(".user-info").First()
	vid.AuthorName = userInfo.Find(".user-nickname").First().Text()
	vid.AuthorUsername = userInfo.Find(".user-username").First().Text()

	if len(vid.AuthorUsername) != 0 && vid.AuthorUsername[0] == '@' {
		vid.AuthorUsername = vid.AuthorUsername[1:]
	}

	return nil
}

type videoObjectJSON struct {
	ContentURL string `json:"contentUrl"`
	Name       string `json:"name"`
	Audio      struct {
		Name   string `json:"name"`
		Author string `json:"author"`
		Entity struct {
			ID string `json:"@id"`
		} `json:"mainEntityOfPage"`
	} `json:"audio"`
	Creator struct {
		Name          string `json:"name"`
		AlternateName string `json:"alternateName"`
	} `json:"creator"`
}

func crawlVideoJSON(vid *crawledVideo, sel *goquery.Selection) error {
	jNode := sel.Find("#videoObject").First()

	if jNode == nil {
		return errors.New("cant find videoObject")
	}

	var obj videoObjectJSON
	err := json.Unmarshal([]byte(jNode.Text()), &obj)

	if err != nil {
		return fmt.Errorf("cant parse videoObject: %w", err)
	}

	vid.URL = obj.ContentURL
	vid.Description = obj.Name

	vid.MusicTitle = obj.Audio.Name
	vid.MusicHref = obj.Audio.Entity.ID

	vid.AuthorName = obj.Creator.Name
	vid.AuthorUsername = obj.Creator.AlternateName

	return nil
}

func crawlVideo(client *http.Client, u string) (*crawledVideo, error) {
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status not 200 OK but %s", res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, fmt.Errorf("cant parse document: %w", err)
	}

	var vid crawledVideo

	err = crawlVideoJSON(&vid, doc.Selection)
	if err != nil {
		log.Printf("crawlVideoJSON failed for %v, trying HTML method", u)
		err = crawlVideoHTML(&vid, doc.Selection)
	}

	return &vid, err
}
