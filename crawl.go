package main

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

func unShorten(client300 *http.Client, method string, u *url.URL) (*url.URL, error) {
	req, err := http.NewRequest(method, u.String(), nil)
	if err != nil {
		return nil, err
	}

	res, err := client300.Do(req)
	if err != nil {
		return nil, err
	}

	loc := res.Header.Get("Location")
	_ = res.Body.Close()

	if res.StatusCode != http.StatusMovedPermanently && res.StatusCode != http.StatusFound {
		return nil, fmt.Errorf("status is not 301 or 302 but %s", res.Status)
	}

	if loc == "" {
		return nil, errors.New("location header empty")
	}

	locURL, err := url.Parse(loc)
	if err != nil {
		return nil, err
	}

	return u.ResolveReference(locURL), nil
}

type linkInfo struct {
	Resolved *url.URL
	Video    *crawledVideo
}

func (b *bot) crawl(u *url.URL, allowContentFailure bool) (*linkInfo, error) {
	info := linkInfo{Resolved: u}

	switch {
	case isVideoURL(u):
		vInfo, err := crawlVideo(b.client, u.String())
		if err == nil {
			info.Video = vInfo
		} else if !allowContentFailure {
			return nil, fmt.Errorf("crawlVideo failure: %w", err)
		}
	}

	return &info, nil
}
