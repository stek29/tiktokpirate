package main

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

var linkRegexp = regexp.MustCompile(`\b(?:https?://)?` +
	`(?P<without_scheme>(?:` +
	`vm\.tiktok\.com/(?P<short_id>[^/]+)/?|` + // short (app share) format
	`m\.tiktok\.com/v/(?P<video_id>\d+)\.html|` + // un-shortened (redirect) format
	`(?:www\.)tiktok\.com/@[^/]+/video/(?P<video_id>\d+)/?` + // long (web share) format
	`)\S*)\b`)

const (
	userIDPathPrefix = "/h5/share/usr/"
	mTikTokBaseURL   = "https://m.tiktok.com"
	webTikTokBaseURL = "https://www.tiktok.com"
)

const (
	QLanguage  = "language"
	QTimestamp = "timestamp"
	QMedium    = "utm_medium"
	QUserID    = "user_id"
)

var manualQueryArgs = map[string]struct{}{
	QLanguage:  {},
	QTimestamp: {},
	QMedium:    {},
	QUserID:    {},
}

func appendQueryParts(parts *[]messagePart, q url.Values) {
	p := *parts

	if v := q.Get(QMedium); v != "" {
		p = append(p, messagePart{name: "OS", value: v})
	}
	if v := q.Get(QLanguage); v != "" {
		p = append(p, messagePart{name: "Lang", value: strings.ToUpper(v)})
	}
	if v := q.Get(QUserID); v != "" {
		p = append(p, messagePart{
			name:   "Share Author",
			isLink: true,
			value:  mTikTokBaseURL + userIDPathPrefix + v + ".html",
		})
	}
	if v := q.Get(QTimestamp); v != "" {
		sec, err := strconv.ParseInt(v, 10, 64)
		if err == nil {
			tv := time.Unix(sec, 0).UTC()
			v = tv.Format("15:04:05 MST Jan 05 2006")
		}
		p = append(p, messagePart{name: "Shared at", value: v})
	}

	for k, v := range q {
		if _, ok := manualQueryArgs[k]; !ok || len(v) == 0 {
			p = append(p, messagePart{
				name:  "`" + k + "`",
				value: "`" + v[0] + "`",
			})
		}
	}

	*parts = p
}

func mergeParts(parts []messagePart) string {
	strParts := make([]string, len(parts))
	for i, p := range parts {
		strParts[i] = p.String()
	}
	return strings.Join(strParts, "\n")
}

type mode int

const (
	modeGet = mode(iota)
	modeDump
	modePreview
	modeVideo
)

func (b *bot) handleMessage(m *tb.Message, u *url.URL, mode mode) error {
	info, err := b.crawl(u, true)
	if err != nil {
		return fmt.Errorf("cant fetch link: %w", err)
	}

	switch mode {
	case modeGet, modeDump:
		var parts []messagePart
		if v := info.Video; v != nil {
			appendVideoPart(&parts, v)
			appendVideoInfoParts(&parts, v)
		}

		if mode == modeDump {
			appendQueryParts(&parts, info.Resolved.Query())
		}

		_, err = b.Reply(m, mergeParts(parts), tb.ModeMarkdown)

	case modePreview:
		if info.Video == nil {
			return errors.New("cant get video")
		}
		pURL, err := getPreviewURL(b.VidPreviewURL, info.Video.asPreviewData(b))
		if err != nil {
			return fmt.Errorf("cant generate preview: %w", err)
		}
		_, err = b.Reply(m, "[@"+info.Video.AuthorUsername+"]("+pURL+")", tb.ModeMarkdown)

	default:
		err = errors.New("internal error: invalid mode")
	}
	return err
}

func (b *bot) extractLink(text string) (*url.URL, error) {
	match := linkRegexp.FindStringSubmatch(text)
	if len(match) < 1 {
		return nil, fmt.Errorf("no url match")
	}

	var u *url.URL
	var err error

	// short_id
	if match[2] != "" {
		u, err = url.Parse("https://vm.tiktok.com/" + match[2] + "/")
		if err != nil {
			return nil, fmt.Errorf("invalid url")
		}
		u, err = unShorten(b.client300, "HEAD", u)
		if err != nil {
			return nil, fmt.Errorf("cant unshorten")
		}
	} else {
		u, err = url.Parse("https://" + match[1]) // without_scheme
		if err != nil {
			return nil, fmt.Errorf("invalid url")
		}
	}

	return u, nil
}

func (b *bot) onMessageWithMode(mode mode) func(m *tb.Message) {
	return func(m *tb.Message) {
		u, err := b.extractLink(m.Text)
		if err != nil {
			if m.Private() {
				b.onHelp(m)
			}
			return
		}

		err = b.handleMessage(m, u, mode)
		if err != nil {
			_, err = b.Reply(m, "error: "+err.Error())
		}
	}
}
