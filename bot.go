package main

import (
	"net/http"

	tb "gopkg.in/tucnak/telebot.v2"
)

type bot struct {
	*tb.Bot
	client        *http.Client
	client300     *http.Client
	VidPreviewURL string
}

// copies client internally
func newBot(tb *tb.Bot, client *http.Client, vidPreviewURL string) *bot {
	if client == nil {
		client = http.DefaultClient
	}

	client300 := *client
	client300.CheckRedirect = noRedirect

	return (&bot{
		Bot:           tb,
		client:        client,
		client300:     &client300,
		VidPreviewURL: vidPreviewURL,
	}).withHandlers()
}

// withHandlers sets all handlers on bot
func (b *bot) withHandlers() *bot {
	b.Handle("/start", b.onHelp)
	b.Handle("/help", b.onHelp)

	b.Handle("/get", b.onMessageWithMode(modeGet))
	b.Handle("/dump", b.onMessageWithMode(modeDump))
	b.Handle(tb.OnText, b.onMessageWithMode(modePreview))

	b.Handle(tb.OnQuery, b.onQuery)

	return b
}
