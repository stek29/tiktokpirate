package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

const (
	teleTokenEnv = "TELEGRAM_BOT_TOKEN"
	userAgentEnv = "USER_AGENT"

	publicURLEnv           = "PUBLIC_URL"
	defaultLongPollTimeout = 10 * time.Second

	pollTypeEnv = "POLL_TYPE"

	pollTypeWebhook  = "webhook"
	pollTypeLongpoll = "longpoll"

	defaultPollType = pollTypeLongpoll

	webhookPattern = "/webhook/"

	shutdownTimeout = 5 * time.Second
)

func main() {
	teleToken := os.Getenv(teleTokenEnv)
	publicURL := os.Getenv(publicURLEnv)
	pollType := os.Getenv(pollTypeEnv)

	if teleToken == "" {
		log.Fatalf("%s should be set", teleTokenEnv)
		return // unreachable
	}

	var srv *http.Server
	var mux *http.ServeMux

	if publicURL == "" {
		log.Printf("warn: %s unset, cant use webhook or previews", publicURLEnv)
	} else {
		srv, mux = getServerMux()

		mux.Handle(vidPreviewPattern, http.HandlerFunc(handleVidPreview))
	}

	tgBot, err := tb.NewBot(tb.Settings{Token: teleToken})
	if err != nil {
		log.Fatal("Failed to create tg bot: ", err)
		return // unreachable
	}

	switch pollType {
	case pollTypeLongpoll:
		tgBot.Poller = &tb.LongPoller{Timeout: defaultLongPollTimeout}
	case pollTypeWebhook:
		if srv == nil {
			log.Fatalf("cant use %s=%s without %s", pollTypeEnv, pollType, publicURLEnv)
			return // unreachable
		}

		wh := &tb.Webhook{
			Endpoint: &tb.WebhookEndpoint{
				PublicURL: publicURL + webhookPattern,
			},
		}

		mux.Handle(webhookPattern, wh)
		tgBot.Poller = wh
	default:
		log.Fatalf("%s should be %s or %s, not `%s`",
			pollTypeEnv, pollTypeLongpoll, pollTypeWebhook, pollType)
	}

	client := getClient()
	var vidPreviewURL string
	if srv != nil {
		vidPreviewURL = publicURL + vidPreviewPattern
	}

	bot := newBot(tgBot, client, vidPreviewURL)

	var wg sync.WaitGroup

	// start server if needed
	if srv != nil {
		wg.Add(1)
		go func() {
			if err := srv.ListenAndServe(); err != nil {
				log.Println("server ListenAndServe error", err)
			}
			wg.Done()
		}()
	}

	wg.Add(1)
	go func() {
		log.Println("bot starting")
		bot.Start()
		log.Println("bot stopped")
		wg.Done()
	}()

	// set up signal handling
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop

	// stop the bot
	bot.Stop()

	// stop the server if needed
	if srv != nil {
		ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		if srv != nil {
			if err := srv.Shutdown(ctx); err != nil {
				log.Println("server Shutdown error")
			}
			log.Println("server shut down")
		}
	}

	wg.Wait()
}
