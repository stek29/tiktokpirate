package main

type messagePart struct {
	name, value string
	isLink      bool
}

func (p messagePart) String() string {
	switch {
	case p.isLink:
		return "[" + p.name + "](" + p.value + ")"
	case p.name == "":
		return p.value
	default:
		return p.name + ": " + p.value
	}
}
